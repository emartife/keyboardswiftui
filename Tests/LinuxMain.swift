import XCTest

import KeyboardSwiftUITests

var tests = [XCTestCaseEntry]()
tests += KeyboardSwiftUITests.allTests()
XCTMain(tests)
